/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    //pkg: grunt.file.readJSON('package.json'),
    meta: {
      version: '0.1.0',
      banner: '/*! Spil Game #2 - v<%= meta.version %> - ' +
        'author: <j.siemiatkowsi@gmail.com> */'
    },
    jshint: {
      // define the files to lint
      all: ['Gruntfile.js', 'js/*.js'],
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: false,
        boss: true,
        eqnull: true,
        browser: true
      },
      globals: {
        CG: false
      }
    },
    concat: {
      dist: {
        src: ['<banner:meta.banner>', 'js/init.js', 'js/touch.js', 'js/preloader.js',
        'js/drawable.js', 'js/background.js', 'js/animate.js', 'js/entity.js', 'js/player.js',
        'js/enemy.js', 'js/main.js'],
        dest: 'js/game.js'
      }
    },
    uglify: {
      options: {
        banner: '/*! Spil Game #2 - v<%= meta.version %> - ' +
        'author: <j.siemiatkowsi@gmail.com> */'
      },
      dist: {
        files: {
          'js/game.min.js': ['<%= concat.dist.dest %>']
        }
      }
    }
    // watch: {
    //   files: '<config:lint.files>',
    //   tasks: 'lint qunit'
    // },
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.registerTask('default', ['jshint', 'concat', 'uglify']);

};
