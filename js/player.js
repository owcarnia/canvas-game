(function() {
    CG.Player = function(image, settings) {
        var me = this,
            life,
            damage,
            sprite,
            colliding,
            movingX,
            movingY;

        this.restart = function() {
            life        = settings.life;
            damage      = settings.damage;
            colliding   = false;
            movingX     = false;
            movingY     = false;

            me.collisions = settings.collisions;
            me.pos        = null;
            me.killed     = false;
            me.target     = null;
        };

        this.init = function() {
            me.restart();

            sprite = {
                image: image,
                width: 150,
                height: 140
            };
            me.anim = {
                idle : new CG.Animation(),
                move : new CG.Animation(),
                attack : new CG.Animation()
            };

            initAnimations();
        };

        function initAnimations() {
            me.anim.idle.init(sprite, 1, [0]);
            me.anim.move.init(sprite, 0.1, [1,0,2,0]);
            me.anim.attack.init(sprite, 0.02, 
                [0,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,18,
                18,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,0]
            );
        }

        this.respawnPlayer = function(){
            me.pos = {
                x: 50,
                y: 250
            };

            me.currentAnim = me.anim.idle;
            me.currentAnim.start();
        };

        this.update = function() {
            var touch = CG.Touch,
                canvas = CG.GAME_CANVAS;

            me.checkCollisions();

            if (touch.tap || touch.doubletap) {
                me.target = {
                    x: touch.tap.center.pageX - touch.offsetLeft,
                    y: touch.tap.center.pageY - touch.offsetTop
                };

                if (touch.doubletap) {
                    if (colliding.length) {
                        //just attack first colliding entity
                        me.moveToStop();
                        me.hit(colliding[0].entity);
                    } else {
                        me.moveToTarget();
                    }
                } else {
                    me.moveToTarget();
                }
            } else if (me.target) {
                me.moveToTarget();
            }

            me.currentAnim.update();  
        };

        this.moveToTarget = function() {
            var newX       = me.target.x,
                newY       = me.target.y,
                x          = me.pos.x,
                y          = me.pos.y;

            movingX = true;

            if (newX > x) {
                me.pos.x += 1;
            } else if (newX < x){
                me.pos.x -= 1;
            } else {
                movingX = false;
            }

            movingY = true;

            if (newY > y) {
                me.pos.y += 1;
            } else if (newY < y) {
                me.pos.y -= 1;
            } else {
                movingY = false;
            }

            if (!movingX && !movingY) {
                me.target = null;
            }

            checkAnimation();
        };

        this.moveToStop = function(){
            movingX = movingY = false;
            me.target = null;

            checkAnimation();
        };

        function checkAnimation() {
            if (movingX || movingY) {
                 if (me.currentAnim !== me.anim.move) {
                    me.currentAnim.stop();
                    me.currentAnim = me.anim.move;
                    me.currentAnim.start();
                }             
            } else {
                if (me.currentAnim !== me.anim.idle) {
                    me.currentAnim.stop();
                    me.currentAnim = me.anim.idle;
                    me.currentAnim.start();
                }
            }
        }

        this.getEntityCollisionBounds = function(){
            return {
                x : me.pos.x,
                y : me.pos.y,
                width: me.collisions.width,
                height: me.collisions.height
            };
        };

        this.checkCollisions = function(x, y) {
            var thisBounds = me.getEntityCollisionBounds(),
                colDir = 0,
                entity,
                bounds;

            colliding = [];

            for (var i=0, l=CG.items.length; i<l; i+=1) {
                entity = CG.items[i];

                if (entity === me || !(entity instanceof CG.Entity)) {
                    continue;
                }

                bounds = entity.getEntityCollisionBounds();

                if (me.collision(thisBounds, bounds, entity)){
                    colliding.push({
                        entity: entity
                    });
                }
            }
        };

        function collides(x, y, r, b, x2, y2, r2, b2, entity) {
            var collision = !(r <= x2 || x > r2 || b <= y2 || y > b2);

            if (collision) {
                entity.onCollision(me);
            } else {
                entity.onCollision(me, true);
            }

            return collision;
        }

        this.collision = function(myBounds, entityBounds, entity) {
            return collides(myBounds.x, myBounds.y, myBounds.x + myBounds.width,
                myBounds.y + myBounds.height, entityBounds.x, entityBounds.y, 
                entityBounds.x + entityBounds.width, entityBounds.y + entityBounds.height, entity);
        };

        this.hit = function(entity) {
            me.currentAnim = me.anim.attack;

            //Particles
            CG.Proton.emitters[0].p.x = me.pos.x;
            CG.Proton.emitters[0].p.y = me.pos.y;
            CG.Proton.emitters[0].emit('once');

            entity.onHit(damage);
            if (entity.killed) {
                me.currentAnim = me.anim.idle;
            }
        };

        this.onHit = function(damage) {
            life -= damage;

            if (life <= 0){
                me.killed = true;

                if (!CG.restarting) {
                    CG.restart();
                }
            }
        }

        this.draw = function() {
            CG.Proton.update();
            
            if (!me.killed) {
                me.currentAnim.draw(me.pos.x, me.pos.y, me.collisions);
                me.drawHealthBar();
            } else {
                me.currentAnim.stop();
            }
        };

        this.drawHealthBar = function() {
            var ctx = CG.GAME_CTX,
                width = (life/settings.life),
                posX = me.pos.x,
                posY = me.pos.y - me.collisions.offsetY;

            ctx.lineWidth = 1;
            ctx.fillStyle = "#FF2A00";
            ctx.fillRect(posX-5, posY-10, 40, 5);
            ctx.fillStyle = "#2AFF00";
            ctx.fillRect(posX-5, posY-10, (40*width), 5);
        };
    };

    CG.Player.prototype = new CG.Entity();
})();