(function() {
    CG.Preloader = function() {
        var me = this,
            imagesList = [],
            imagesLeft = null,
            imagesLoaded = 0;

        // we assume images is always an array
        this.init = function(images) {
            for (var i=0, l=images.length; i<l; i+=1){
                imagesList.push(images[i]);
            }

            me.preload();
        };

        this.preload = function() { 
            imagesLeft = imagesList.length;

            var img;

            for (var i=0, l=imagesList.length; i<l; i+=1){
                img = new Image();
                img.onload = imageOnLoaded;
                img.src = imagesList[i].src;
                CG.images[imagesList[i].name] = img;
            }
        };

        function imageOnLoaded() {
            imagesLoaded++;

            if (imagesLoaded === imagesLeft) {
                CG.initEntities();
            }
        }
    };
})();