(function() {
    var me = CG;

    me.init = function(width, height, debug) {
        if (debug) {
            me._DEBUG = true;
        }

        me.GAME_CANVAS = document.getElementById('canvas');
        me.GAME_CTX = me.GAME_CANVAS.getContext('2d');
        me.BG_CANVAS = document.getElementById('bgCanvas');
        me.BG_CTX = me.BG_CANVAS.getContext('2d');

        // Setting hardware scaling 
        me.GAME_CANVAS.width = width; 
        me.GAME_CANVAS.height = height; 
        me.BG_CANVAS.width = width; 
        me.BG_CANVAS.height = height; 

        me.GAME_CANVAS.setAttribute('tabindex', '0');
        me.GAME_CANVAS.focus();

        var preloader = new CG.Preloader();

        preloader.init([
            { src: "img/bg.png", name: 'background' },
            { src: "img/player.png", name: 'player' },
            { src: "img/zombie.png", name: 'monster'},
            { src: "img/blood.png", name: 'blood'}
        ]);
    };

    function createProton() {
        var proton = new Proton,
            emitter = new Proton.Emitter();

        emitter.rate = new Proton.Rate(new Proton.Span(16, 18), new Proton.Span(.05, .2));
        emitter.addInitialize(new Proton.Mass(1));
        emitter.addInitialize(new Proton.ImageTarget(CG.images.blood));
        emitter.addInitialize(new Proton.Life(1));
        emitter.addInitialize(new Proton.Velocity(new Proton.Span(1, 2), new Proton.Span(0, 80, true), 'polar'));
        emitter.addBehaviour(new Proton.Alpha(1, 0));
        emitter.addBehaviour(new Proton.Gravity(3));
        emitter.addBehaviour(new Proton.Rotate());
        emitter.addBehaviour(new Proton.Scale(0, Proton.getSpan(0.3, 0.5), Infinity, Proton.easeOutCubic));
        proton.addEmitter(emitter);

        var renderer = new Proton.Renderer('canvas', proton, CG.GAME_CANVAS);
        renderer.start();

        CG.Proton = proton;
    }

    me.initEntities = function() {
        if (!me.restarting) {
            var bg = new CG.Background(CG.images.background);

            monster = new CG.Enemy(CG.images.monster, {
                life: 40,
                damage: 20,
                collisions: {
                    offsetX: 5,
                    offsetY: 60,
                    width: 50,
                    height: 20
                }
            });
            me.player = me.player || new CG.Player(CG.images.player, {
                life: 200,
                damage: 20,
                collisions: {
                    offsetX: 55,
                    offsetY: 75,
                    width: 35,
                    height: 25
                }
            });

            me.items.push(bg);
            me.items.push(me.player);
            me.items.push(monster);

            createProton();
            //createRenderer();
            initEvents();
        } else {
            me.restarting = false;
        }

        for (var i=0, l=me.items.length; i<l; i+=1){
            me.items[i].init();
        }

        me.player.respawnPlayer();
        me.loop();
    };

    function initEvents() {
        me.Touch.init();

        Hammer(me.GAME_CANVAS).on("tap", function(event) {
            me.Touch.onTap(event.gesture);
        });

        Hammer(me.GAME_CANVAS).on('doubletap', function(event) {
            me.Touch.onDoubleTap(event.gesture);
        });
    }

    me.clear = function() {
        me.GAME_CANVAS.width = me.GAME_CANVAS.width;
    };

    me.loop = function() {
        me.clear();

        for (var i=0, l = me.items.length; i<l; i+=1){
            me.items[i].update();
            me.items[i].draw();
        }

        me.Touch.clearTouch();

        if (me.restarting) {
            me.GAME_CTX.font = "20pt Arial red";
            me.GAME_CTX.fillText("Restarting...", me.GAME_CANVAS.width/2-100, me.GAME_CANVAS.height/2-50);
        } else {
            requestAnimationFrame(me.loop);
        }    
    };

    me.restart = function(){
        me.restarting = true;

        setTimeout(function(){
            me.initEntities();
        }, 3000);
        
    };
})();

window.onload = function(){
    this.CG.init(500, 500, false);
}