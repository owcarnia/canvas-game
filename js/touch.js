(function() {
    CG.Touch = {
        tap : false,
        doubletap : false,
        offsetLeft: 0,
        offsetTop: 0,

        init : function() {
            var el = CG.GAME_CANVAS;

            while (el.parentElement) {
                this.offsetLeft += el.offsetLeft;
                this.offsetTop += el.offsetTop;

                el = el.parentElement;
            }
        },

        onTap : function(gesture) {
            this.tap = gesture;
        },

        onDoubleTap : function(gesture){
            this.doubletap = gesture;
        },

        clearTouch : function(){
            this.tap = false;
            this.doubletap = false;
        }
    };
})();