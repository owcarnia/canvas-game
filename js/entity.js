(function(){
    CG.Entity = function() {
        this.anim = null;
        this.currentAnim = null;

        this.init = function() {
        };

        this.update = function() {
        };

        this.draw = function() {
        };
    };

    CG.Entity.prototype = new CG.Drawable();
})();