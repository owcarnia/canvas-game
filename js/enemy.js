(function(){
    CG.Enemy = function(image, settings) {
        var me = this,
            life,
            attackTimer,
            sprite,
            damage,
            movingX,
            movingY;

        this.restart = function() {
            life        = settings.life;
            attackTimer = null;
            damage      = settings.damage;
            movingX     = false;
            movingY     = false;

            me.collisions = settings.collisions;
            me.pos        = null;
            me.killed     = false;
            me.target     = null;
        };

        this.init = function() {
            me.restart();

            sprite = {
                image: image,
                width: 60,
                height: 80
            };
            me.anim = {
                idle : new CG.Animation(),
                move : new CG.Animation(),
                attack : new CG.Animation()
            };

            initAnimations();
            me.respawnMonster();
        };

        function initAnimations() {
            me.anim.idle.init(sprite, 1, [0]);
            me.anim.move.init(sprite, 0.3, [1,0,2,0]);
            me.anim.attack.init(sprite, 0.1, [0,2,0,1,3,4,3,5,3,4,3,0]);
        }

        this.respawnMonster = function(){
            me.pos = {
                x: 200,
                y: 100
            };

            me.currentAnim = me.anim.idle;
            me.currentAnim.start();
        };

        this.getTime = function() {
            var time = Date.now() - attackTimer;

            return time/1000;
        };

        this.getEntityCollisionBounds = function(){
            return {
                x : me.pos.x,
                y: me.pos.y,
                width: me.collisions.width,
                height: me.collisions.height
            };
        };

        this.update = function() {
            if (!attackTimer) {
                if (!me.target) {
                    var x = ~~(Math.random()*(-500)+500),
                        y = ~~(Math.random()*(-500)+500);

                    this.target = {
                        x: x,
                        y: y
                    };
                }

                me.moveToTarget();
            }

            this.currentAnim.update();
        };

        this.onHit = function(damage) {
            life -= damage;

            if (life <= 0){
                me.killed = true;

                if (!CG.restarting) {
                    CG.restart();
                }
            }
        };

        this.onCollision = function(entity, stopColliding) {
            if (stopColliding || me.killed) {
                attackTimer = null;
            } else {
                me.moveToStop();
                attackTimer = attackTimer || Date.now();
                me.hit(entity);
            }
        };

        //Monster moves randomly
        this.moveToTarget = function() {
            var newX       = me.target.x,
                newY       = me.target.y,
                x          = me.pos.x,
                y          = me.pos.y;

            movingX = true;

            if (newX > x) {
                me.pos.x += 0.5;
            } else if (newX < x){
                me.pos.x -= 0.5;
            } else {
                movingX = false;
            }

            movingY = true;

            if (newY > y) {
                me.pos.y += 0.5;
            } else if (newY < y) {
                me.pos.y -= 0.5;
            } else {
                movingY = false;
            }

            if (!movingX && !movingY) {
                me.target = null;
            }

            checkAnimation();
        };

        function checkAnimation() {

            if (movingX || movingY) {
                 if (me.currentAnim !== me.anim.move) {
                    me.currentAnim.stop();
                    me.currentAnim = me.anim.move;
                    me.currentAnim.start();
                }             
            } else {
                if (me.currentAnim !== me.anim.idle) {
                    me.currentAnim.stop();
                    me.currentAnim = me.anim.idle;
                    me.currentAnim.start();
                }
            }
        }

        this.moveToStop = function(){
            movingX = movingY = false;
            me.target = null;

            checkAnimation();
        };

        this.hit = function(entity) {
            var time = me.getTime();

            if (time >= 0.5) {
                attackTimer = Date.now();
                me.currentAnim = me.anim.attack;
                entity.onHit(damage);
                if (entity.killed) {
                    me.currentAnim = me.anim.idle;
                }
            }
        };

        this.draw = function() {
            if (!this.killed) {
                me.currentAnim.draw(me.pos.x, me.pos.y, me.collisions);
                me.drawHealthBar();
            } else {
                me.currentAnim.stop();
            }
        };

        this.drawHealthBar = function() {
            var ctx = CG.GAME_CTX,
                width = (life/settings.life),
                posX = me.pos.x,
                posY = me.pos.y - me.collisions.offsetY;

            ctx.lineWidth = 1;
            ctx.fillStyle = "#FF2A00";
            ctx.fillRect(posX+5, posY-10, 40, 5);
            ctx.fillStyle = "#2AFF00";
            ctx.fillRect(posX+5, posY-10, (40*width), 5);
        };
    };

    CG.Enemy.prototype = new CG.Entity();
})();