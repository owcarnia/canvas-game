(function() {
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    var Game = function() {
        var me = this;

        me.items = [];
        me.images = {};
    };

    window.CG = new Game();
})();
(function() {
    CG.Touch = {
        tap : false,
        doubletap : false,
        offsetLeft: 0,
        offsetTop: 0,

        init : function() {
            var el = CG.GAME_CANVAS;

            while (el.parentElement) {
                this.offsetLeft += el.offsetLeft;
                this.offsetTop += el.offsetTop;

                el = el.parentElement;
            }
        },

        onTap : function(gesture) {
            this.tap = gesture;
        },

        onDoubleTap : function(gesture){
            this.doubletap = gesture;
        },

        clearTouch : function(){
            this.tap = false;
            this.doubletap = false;
        }
    };
})();
(function() {
    CG.Preloader = function() {
        var me = this,
            imagesList = [],
            imagesLeft = null,
            imagesLoaded = 0;

        // we assume images is always an array
        this.init = function(images) {
            for (var i=0, l=images.length; i<l; i+=1){
                imagesList.push(images[i]);
            }

            me.preload();
        };

        this.preload = function() { 
            imagesLeft = imagesList.length;

            var img;

            for (var i=0, l=imagesList.length; i<l; i+=1){
                img = new Image();
                img.onload = imageOnLoaded;
                img.src = imagesList[i].src;
                CG.images[imagesList[i].name] = img;
            }
        };

        function imageOnLoaded() {
            imagesLoaded++;

            if (imagesLoaded === imagesLeft) {
                CG.initEntities();
            }
        }
    };
})();
(function(){
    CG.Drawable = function() {

        this.init = function() {
        };

        this.update = function() {     
        };

        this.draw = function() {
        };
    };
})();
(function() {
    CG.Background = function(image) {
        var me = this,
            canvas = CG.BG_CANVAS,
            ctx = CG.BG_CTX,
            bgData = false;

        this.init = function() {
            if (!bgData){
                createBg();
            }
        };

        function createBg(){
            var tileWidth  = image.width,
                tileHeight = image.height,
                cvsWidth   = canvas.width,
                cvsHeight  = canvas.height;

            var l1 = Math.ceil(cvsWidth/tileWidth);
            var l2 = Math.ceil(cvsHeight/tileHeight);

            for (var i=0; i<l1; i+=1) {
                for (var j=0; j<l2; j+=1){
                    ctx.drawImage(image, i*tileWidth, j*tileHeight);
                }
            }

            bgData = true;
        }
    };

    CG.Background.prototype = new CG.Drawable();
})();
(function() {
    CG.Animation =  function(){
        var me = this,
            image = null,
            width = null,
            height = null,
            frameTime = 0,
            frames = null,
            timer = null,
            tile = null,
            frame = 0,
            ctx = null,
            stop = null;

        this.init = function(sprite, singleFrame, sequence) {
            image = sprite.image;
            width = sprite.width;
            height = sprite.height;
            frameTime = singleFrame;
            frames = sequence;
            tile = frames[0];
            ctx = CG.GAME_CTX;
        };

        this.getTime = function() {
            var time = Date.now() - timer;

            return time/1000;
        };

        this.stop = function() {
            stop = 1;
        };

        this.start = function() {
            frame = 0;
            tile  = frames[0];
            timer = Date.now();
            stop  = 0;
        };

        this.update = function(dt) {
            var time = me.getTime();

            if (time >= frameTime) {
                timer = Date.now();

                if (frames.length > 1 && frame < (frames.length-1)){
                    frame += 1;
                } else {
                    frame = 0;
                }
            }

            tile = frames[frame];
        };

        this.draw = function(targetX, targetY, collisions) {
            if (me.stop) {
                var x = tile * width;

                if (collisions) {
                    targetX -= collisions.offsetX;
                    targetY -= collisions.offsetY;
                }

                ctx.drawImage(
                    image,
                    x, 0,
                    width, height,
                    targetX, targetY,
                    width, height
                );

                if (CG._DEBUG){
                    ctx.strokeStyle = "#000";
                    ctx.lineWidth = 1;
                    ctx.strokeRect(targetX, targetY, width, height);
                    ctx.strokeStyle = "red";
                    ctx.strokeRect(targetX + collisions.offsetX, targetY + collisions.offsetY, 
                        collisions.width, collisions.height);
                    ctx.strokeStyle = "#000";
                }
            }
        };

        CG.Animation.prototype = new CG.Drawable();
    };
})();
(function(){
    CG.Entity = function() {
        this.anim = null;
        this.currentAnim = null;

        this.init = function() {
        };

        this.update = function() {
        };

        this.draw = function() {
        };
    };

    CG.Entity.prototype = new CG.Drawable();
})();
(function() {
    CG.Player = function(image, settings) {
        var me = this,
            life,
            damage,
            sprite,
            colliding,
            movingX,
            movingY;

        this.restart = function() {
            life        = settings.life;
            damage      = settings.damage;
            colliding   = false;
            movingX     = false;
            movingY     = false;

            me.collisions = settings.collisions;
            me.pos        = null;
            me.killed     = false;
            me.target     = null;
        };

        this.init = function() {
            me.restart();

            sprite = {
                image: image,
                width: 150,
                height: 140
            };
            me.anim = {
                idle : new CG.Animation(),
                move : new CG.Animation(),
                attack : new CG.Animation()
            };

            initAnimations();
        };

        function initAnimations() {
            me.anim.idle.init(sprite, 1, [0]);
            me.anim.move.init(sprite, 0.1, [1,0,2,0]);
            me.anim.attack.init(sprite, 0.02, 
                [0,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,18,
                18,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,0]
            );
        }

        this.respawnPlayer = function(){
            me.pos = {
                x: 50,
                y: 250
            };

            me.currentAnim = me.anim.idle;
            me.currentAnim.start();
        };

        this.update = function() {
            var touch = CG.Touch,
                canvas = CG.GAME_CANVAS;

            me.checkCollisions();

            if (touch.tap || touch.doubletap) {
                me.target = {
                    x: touch.tap.center.pageX - touch.offsetLeft,
                    y: touch.tap.center.pageY - touch.offsetTop
                };

                if (touch.doubletap) {
                    if (colliding.length) {
                        //just attack first colliding entity
                        me.moveToStop();
                        me.hit(colliding[0].entity);
                    } else {
                        me.moveToTarget();
                    }
                } else {
                    me.moveToTarget();
                }
            } else if (me.target) {
                me.moveToTarget();
            }

            me.currentAnim.update();  
        };

        this.moveToTarget = function() {
            var newX       = me.target.x,
                newY       = me.target.y,
                x          = me.pos.x,
                y          = me.pos.y;

            movingX = true;

            if (newX > x) {
                me.pos.x += 1;
            } else if (newX < x){
                me.pos.x -= 1;
            } else {
                movingX = false;
            }

            movingY = true;

            if (newY > y) {
                me.pos.y += 1;
            } else if (newY < y) {
                me.pos.y -= 1;
            } else {
                movingY = false;
            }

            if (!movingX && !movingY) {
                me.target = null;
            }

            checkAnimation();
        };

        this.moveToStop = function(){
            movingX = movingY = false;
            me.target = null;

            checkAnimation();
        };

        function checkAnimation() {
            if (movingX || movingY) {
                 if (me.currentAnim !== me.anim.move) {
                    me.currentAnim.stop();
                    me.currentAnim = me.anim.move;
                    me.currentAnim.start();
                }             
            } else {
                if (me.currentAnim !== me.anim.idle) {
                    me.currentAnim.stop();
                    me.currentAnim = me.anim.idle;
                    me.currentAnim.start();
                }
            }
        }

        this.getEntityCollisionBounds = function(){
            return {
                x : me.pos.x,
                y : me.pos.y,
                width: me.collisions.width,
                height: me.collisions.height
            };
        };

        this.checkCollisions = function(x, y) {
            var thisBounds = me.getEntityCollisionBounds(),
                colDir = 0,
                entity,
                bounds;

            colliding = [];

            for (var i=0, l=CG.items.length; i<l; i+=1) {
                entity = CG.items[i];

                if (entity === me || !(entity instanceof CG.Entity)) {
                    continue;
                }

                bounds = entity.getEntityCollisionBounds();

                if (me.collision(thisBounds, bounds, entity)){
                    colliding.push({
                        entity: entity
                    });
                }
            }
        };

        function collides(x, y, r, b, x2, y2, r2, b2, entity) {
            var collision = !(r <= x2 || x > r2 || b <= y2 || y > b2);

            if (collision) {
                entity.onCollision(me);
            } else {
                entity.onCollision(me, true);
            }

            return collision;
        }

        this.collision = function(myBounds, entityBounds, entity) {
            return collides(myBounds.x, myBounds.y, myBounds.x + myBounds.width,
                myBounds.y + myBounds.height, entityBounds.x, entityBounds.y, 
                entityBounds.x + entityBounds.width, entityBounds.y + entityBounds.height, entity);
        };

        this.hit = function(entity) {
            me.currentAnim = me.anim.attack;

            //Particles
            CG.Proton.emitters[0].p.x = me.pos.x;
            CG.Proton.emitters[0].p.y = me.pos.y;
            CG.Proton.emitters[0].emit('once');

            entity.onHit(damage);
            if (entity.killed) {
                me.currentAnim = me.anim.idle;
            }
        };

        this.onHit = function(damage) {
            life -= damage;

            if (life <= 0){
                me.killed = true;

                if (!CG.restarting) {
                    CG.restart();
                }
            }
        }

        this.draw = function() {
            CG.Proton.update();
            
            if (!me.killed) {
                me.currentAnim.draw(me.pos.x, me.pos.y, me.collisions);
                me.drawHealthBar();
            } else {
                me.currentAnim.stop();
            }
        };

        this.drawHealthBar = function() {
            var ctx = CG.GAME_CTX,
                width = (life/settings.life),
                posX = me.pos.x,
                posY = me.pos.y - me.collisions.offsetY;

            ctx.lineWidth = 1;
            ctx.fillStyle = "#FF2A00";
            ctx.fillRect(posX-5, posY-10, 40, 5);
            ctx.fillStyle = "#2AFF00";
            ctx.fillRect(posX-5, posY-10, (40*width), 5);
        };
    };

    CG.Player.prototype = new CG.Entity();
})();
(function(){
    CG.Enemy = function(image, settings) {
        var me = this,
            life,
            attackTimer,
            sprite,
            damage,
            movingX,
            movingY;

        this.restart = function() {
            life        = settings.life;
            attackTimer = null;
            damage      = settings.damage;
            movingX     = false;
            movingY     = false;

            me.collisions = settings.collisions;
            me.pos        = null;
            me.killed     = false;
            me.target     = null;
        };

        this.init = function() {
            me.restart();

            sprite = {
                image: image,
                width: 60,
                height: 80
            };
            me.anim = {
                idle : new CG.Animation(),
                move : new CG.Animation(),
                attack : new CG.Animation()
            };

            initAnimations();
            me.respawnMonster();
        };

        function initAnimations() {
            me.anim.idle.init(sprite, 1, [0]);
            me.anim.move.init(sprite, 0.3, [1,0,2,0]);
            me.anim.attack.init(sprite, 0.1, [0,2,0,1,3,4,3,5,3,4,3,0]);
        }

        this.respawnMonster = function(){
            me.pos = {
                x: 200,
                y: 100
            };

            me.currentAnim = me.anim.idle;
            me.currentAnim.start();
        };

        this.getTime = function() {
            var time = Date.now() - attackTimer;

            return time/1000;
        };

        this.getEntityCollisionBounds = function(){
            return {
                x : me.pos.x,
                y: me.pos.y,
                width: me.collisions.width,
                height: me.collisions.height
            };
        };

        this.update = function() {
            if (!attackTimer) {
                if (!me.target) {
                    var x = ~~(Math.random()*(-500)+500),
                        y = ~~(Math.random()*(-500)+500);

                    this.target = {
                        x: x,
                        y: y
                    };
                }

                me.moveToTarget();
            }

            this.currentAnim.update();
        };

        this.onHit = function(damage) {
            life -= damage;

            if (life <= 0){
                me.killed = true;

                if (!CG.restarting) {
                    CG.restart();
                }
            }
        };

        this.onCollision = function(entity, stopColliding) {
            if (stopColliding || me.killed) {
                attackTimer = null;
            } else {
                me.moveToStop();
                attackTimer = attackTimer || Date.now();
                me.hit(entity);
            }
        };

        //Monster moves randomly
        this.moveToTarget = function() {
            var newX       = me.target.x,
                newY       = me.target.y,
                x          = me.pos.x,
                y          = me.pos.y;

            movingX = true;

            if (newX > x) {
                me.pos.x += 0.5;
            } else if (newX < x){
                me.pos.x -= 0.5;
            } else {
                movingX = false;
            }

            movingY = true;

            if (newY > y) {
                me.pos.y += 0.5;
            } else if (newY < y) {
                me.pos.y -= 0.5;
            } else {
                movingY = false;
            }

            if (!movingX && !movingY) {
                me.target = null;
            }

            checkAnimation();
        };

        function checkAnimation() {

            if (movingX || movingY) {
                 if (me.currentAnim !== me.anim.move) {
                    me.currentAnim.stop();
                    me.currentAnim = me.anim.move;
                    me.currentAnim.start();
                }             
            } else {
                if (me.currentAnim !== me.anim.idle) {
                    me.currentAnim.stop();
                    me.currentAnim = me.anim.idle;
                    me.currentAnim.start();
                }
            }
        }

        this.moveToStop = function(){
            movingX = movingY = false;
            me.target = null;

            checkAnimation();
        };

        this.hit = function(entity) {
            var time = me.getTime();

            if (time >= 0.5) {
                attackTimer = Date.now();
                me.currentAnim = me.anim.attack;
                entity.onHit(damage);
                if (entity.killed) {
                    me.currentAnim = me.anim.idle;
                }
            }
        };

        this.draw = function() {
            if (!this.killed) {
                me.currentAnim.draw(me.pos.x, me.pos.y, me.collisions);
                me.drawHealthBar();
            } else {
                me.currentAnim.stop();
            }
        };

        this.drawHealthBar = function() {
            var ctx = CG.GAME_CTX,
                width = (life/settings.life),
                posX = me.pos.x,
                posY = me.pos.y - me.collisions.offsetY;

            ctx.lineWidth = 1;
            ctx.fillStyle = "#FF2A00";
            ctx.fillRect(posX+5, posY-10, 40, 5);
            ctx.fillStyle = "#2AFF00";
            ctx.fillRect(posX+5, posY-10, (40*width), 5);
        };
    };

    CG.Enemy.prototype = new CG.Entity();
})();
(function() {
    var me = CG;

    me.init = function(width, height, debug) {
        if (debug) {
            me._DEBUG = true;
        }

        me.GAME_CANVAS = document.getElementById('canvas');
        me.GAME_CTX = me.GAME_CANVAS.getContext('2d');
        me.BG_CANVAS = document.getElementById('bgCanvas');
        me.BG_CTX = me.BG_CANVAS.getContext('2d');

        // Setting hardware scaling 
        me.GAME_CANVAS.width = width; 
        me.GAME_CANVAS.height = height; 
        me.BG_CANVAS.width = width; 
        me.BG_CANVAS.height = height; 

        me.GAME_CANVAS.setAttribute('tabindex', '0');
        me.GAME_CANVAS.focus();

        var preloader = new CG.Preloader();

        preloader.init([
            { src: "img/bg.png", name: 'background' },
            { src: "img/player.png", name: 'player' },
            { src: "img/zombie.png", name: 'monster'},
            { src: "img/blood.png", name: 'blood'}
        ]);
    };

    function createProton() {
        var proton = new Proton,
            emitter = new Proton.Emitter();

        emitter.rate = new Proton.Rate(new Proton.Span(16, 18), new Proton.Span(.05, .2));
        emitter.addInitialize(new Proton.Mass(1));
        emitter.addInitialize(new Proton.ImageTarget(CG.images.blood));
        emitter.addInitialize(new Proton.Life(1));
        emitter.addInitialize(new Proton.Velocity(new Proton.Span(1, 2), new Proton.Span(0, 80, true), 'polar'));
        emitter.addBehaviour(new Proton.Alpha(1, 0));
        emitter.addBehaviour(new Proton.Gravity(3));
        emitter.addBehaviour(new Proton.Rotate());
        emitter.addBehaviour(new Proton.Scale(0, Proton.getSpan(0.3, 0.5), Infinity, Proton.easeOutCubic));
        proton.addEmitter(emitter);

        var renderer = new Proton.Renderer('canvas', proton, CG.GAME_CANVAS);
        renderer.start();

        CG.Proton = proton;
    }

    me.initEntities = function() {
        if (!me.restarting) {
            var bg = new CG.Background(CG.images.background);

            monster = new CG.Enemy(CG.images.monster, {
                life: 40,
                damage: 20,
                collisions: {
                    offsetX: 5,
                    offsetY: 60,
                    width: 50,
                    height: 20
                }
            });
            me.player = me.player || new CG.Player(CG.images.player, {
                life: 200,
                damage: 20,
                collisions: {
                    offsetX: 55,
                    offsetY: 75,
                    width: 35,
                    height: 25
                }
            });

            me.items.push(bg);
            me.items.push(me.player);
            me.items.push(monster);

            createProton();
            //createRenderer();
            initEvents();
        } else {
            me.restarting = false;
        }

        for (var i=0, l=me.items.length; i<l; i+=1){
            me.items[i].init();
        }

        me.player.respawnPlayer();
        me.loop();
    };

    function initEvents() {
        me.Touch.init();

        Hammer(me.GAME_CANVAS).on("tap", function(event) {
            me.Touch.onTap(event.gesture);
        });

        Hammer(me.GAME_CANVAS).on('doubletap', function(event) {
            me.Touch.onDoubleTap(event.gesture);
        });
    }

    me.clear = function() {
        me.GAME_CANVAS.width = me.GAME_CANVAS.width;
    };

    me.loop = function() {
        me.clear();

        for (var i=0, l = me.items.length; i<l; i+=1){
            me.items[i].update();
            me.items[i].draw();
        }

        me.Touch.clearTouch();

        if (me.restarting) {
            me.GAME_CTX.font = "20pt Arial red";
            me.GAME_CTX.fillText("Restarting...", me.GAME_CANVAS.width/2-100, me.GAME_CANVAS.height/2-50);
        } else {
            requestAnimationFrame(me.loop);
        }    
    };

    me.restart = function(){
        me.restarting = true;

        setTimeout(function(){
            me.initEntities();
        }, 3000);
        
    };
})();

window.onload = function(){
    this.CG.init(500, 500, false);
}