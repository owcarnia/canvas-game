(function() {
    CG.Background = function(image) {
        var me = this,
            canvas = CG.BG_CANVAS,
            ctx = CG.BG_CTX,
            bgData = false;

        this.init = function() {
            if (!bgData){
                createBg();
            }
        };

        function createBg(){
            var tileWidth  = image.width,
                tileHeight = image.height,
                cvsWidth   = canvas.width,
                cvsHeight  = canvas.height;

            var l1 = Math.ceil(cvsWidth/tileWidth);
            var l2 = Math.ceil(cvsHeight/tileHeight);

            for (var i=0; i<l1; i+=1) {
                for (var j=0; j<l2; j+=1){
                    ctx.drawImage(image, i*tileWidth, j*tileHeight);
                }
            }

            bgData = true;
        }
    };

    CG.Background.prototype = new CG.Drawable();
})();