(function() {
    CG.Animation =  function(){
        var me = this,
            image = null,
            width = null,
            height = null,
            frameTime = 0,
            frames = null,
            timer = null,
            tile = null,
            frame = 0,
            ctx = null,
            stop = null;

        this.init = function(sprite, singleFrame, sequence) {
            image = sprite.image;
            width = sprite.width;
            height = sprite.height;
            frameTime = singleFrame;
            frames = sequence;
            tile = frames[0];
            ctx = CG.GAME_CTX;
        };

        this.getTime = function() {
            var time = Date.now() - timer;

            return time/1000;
        };

        this.stop = function() {
            stop = 1;
        };

        this.start = function() {
            frame = 0;
            tile  = frames[0];
            timer = Date.now();
            stop  = 0;
        };

        this.update = function(dt) {
            var time = me.getTime();

            if (time >= frameTime) {
                timer = Date.now();

                if (frames.length > 1 && frame < (frames.length-1)){
                    frame += 1;
                } else {
                    frame = 0;
                }
            }

            tile = frames[frame];
        };

        this.draw = function(targetX, targetY, collisions) {
            if (me.stop) {
                var x = tile * width;

                if (collisions) {
                    targetX -= collisions.offsetX;
                    targetY -= collisions.offsetY;
                }

                ctx.drawImage(
                    image,
                    x, 0,
                    width, height,
                    targetX, targetY,
                    width, height
                );

                if (CG._DEBUG){
                    ctx.strokeStyle = "#000";
                    ctx.lineWidth = 1;
                    ctx.strokeRect(targetX, targetY, width, height);
                    ctx.strokeStyle = "red";
                    ctx.strokeRect(targetX + collisions.offsetX, targetY + collisions.offsetY, 
                        collisions.width, collisions.height);
                    ctx.strokeStyle = "#000";
                }
            }
        };

        CG.Animation.prototype = new CG.Drawable();
    };
})();